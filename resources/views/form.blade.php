@extends('base')

@section('title', 'Adicionar Vaquinha')

@section('content')

<div class="page-header">
    <ol class="breadcrumb">
    	<li><a href="{{ route('home.index') }}">Vaquinhas</a></li>
    	<li class="active">Adicionar</li>
    </ol>
</div>

<form class="form-horizontal" action="{{ route('home.save') }}" method="post" enctype="multipart/form-data">
    {!! csrf_field() !!}
	<fieldset>
    	<div class="form-group">
   			<label class="col-sm-2 control-label" for="weight">Peso:</label>
   			<div class="col-xs-2">
   				<input type="number" name="weight" class="form-control" id="weight" value="" required autofocus />
   			</div>
    	</div>

        <div class="form-group">
   			<label class="col-sm-2 control-label" for="age">Idade:</label>
   			<div class="col-xs-2">
   				<input type="number" name="age" class="form-control" id="age" value="" required />
   			</div>
    	</div>

        <div class="form-group">
   			<label class="col-sm-2 control-label" for="price">Preço:</label>
   			<div class="col-xs-2">
   				<input type="number" name="price" class="form-control" id="price" step="any" value="" required />
   			</div>
    	</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button class="btn btn-primary" type="submit">Salvar</button>
				<a href="/" class="btn btn-default">Voltar</a>
			</div>
		</div>
    </fieldset>
</form>

@endsection
