@extends('base')

@section('title', 'Lista de Vaquinhas')

@section('content')

<div class="page-header">
    <ol class="breadcrumb">
        <li class="active">Vaquinhas</li>
    </ol>
</div>

<div class="action-header">
    <a class="btn btn-primary" href="{{ route('home.add') }}"><i class="glyphicon glyphicon-plus-sign"></i> Adicionar </a>
</div>

<table class="table table-striped">
<thead>
    <tr>
        <th>Peso</th>
        <th width="200">Idade</th>
        <th width="200">Preço</th>
        <th width="200">Pasto Mensal</th>
        <th width="200">Custo Anual</th>
    </tr>
</thead>
<tbody>
    @if(count($items) == 0)
        <tr> <td colspan="3" align="center">Nenhum item cadastrado.</td> </tr>
    @else
        @foreach($items as $item)
            <tr>
                <td> {{ $item->weight }}kg </td>
                <td> {{ $item->age }} </td>
                <td>R$ {{ $item->price }} </td>
                <td> {{ $item->pasture_monthly }}kg </td>
                <td>R$ {{ $item->annual_cost }} </td>
            </tr>
        @endforeach
    @endif
</tbody>
</table>

@endsection
