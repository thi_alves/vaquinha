<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use cURL;

class HomeController extends Controller
{
    public function index() {
        $items = array();
        $curl = new cURL;

        $response = $curl->newRequest('get', 'http://recrutamento.taginterativa.com.br/api/v1/cows')
            ->setHeader('Content-Type', 'application/json')->setHeader('access-token', '8c6e321656')->send();

        if($response->statusCode == 200) {
            $items = json_decode($response->body);
            foreach ($items as $item) {
                $item->pasture_monthly = ((intval($item->weight) * 3) / 100) * 30;
                $item->annual_cost = ($item->pasture_monthly * 12) * 0.2;
            }
            usort($items, function($a, $b) {
                if ($a->annual_cost == $b->annual_cost) return 0;
                return ($a->annual_cost < $b->annual_cost) ? -1 : 1;
            });
        }

        return view('list', compact('items'));
    }

    public function add() {
        return view('form');
    }

    public function save(Request $request) {
        $curl = new cURL;

        $data = array('weight' => intval($request->input('weight')), 'age' => intval($request->input('age')), 'price' => intval($request->input('price')));

        $response = $curl->newJsonRequest('post', 'http://recrutamento.taginterativa.com.br/api/v1/cows', $data)
            ->setHeader('access-token', '8c6e321656')->send();

        return redirect('/');
    }
}
